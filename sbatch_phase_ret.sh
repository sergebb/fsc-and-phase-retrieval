#! /bin/sh

#SBATCH -D /s/ls4/groups/g0065/ikonk/phase_retrieval
#SBATCH -o log/%j.out
#SBATCH -e log/%j.err
#SBATCH -p hpc5-gpu-3d
#SBATCH -n 1
#SBATCH --gres=gpu:1

export HOME=/s/ls4/users/sbobkov

ANACONDA_DIR=/s/ls4/users/sbobkov/anaconda2
SW_HOME=/s/ls4/users/sbobkov/sw

export LD_LIBRARY_PATH=$ANACONDA_DIR/lib:$SW_HOME/lib:/s/ls4/sw/misc/lib:/s/ls4/sw/misc/lib64:$LD_LIBRARY_PATH
export PATH=$ANACONDA_DIR/bin:$PATH

module load intel-compilers cuda/9.0 

/s/ls4/sw/glibc/2.17/lib/ld-linux-x86-64.so.2 --library-path /s/ls4/sw/glibc/2.17/lib:$LD_LIBRARY_PATH `which python` phase_ret.py $1 -m 10 -M 150 -b 2

#Model:
#/s/ls4/sw/glibc/2.17/lib/ld-linux-x86-64.so.2 --library-path /s/ls4/sw/glibc/2.17/lib:$LD_LIBRARY_PATH `which python` phase_ret.py $1 -m 0 -M 300 -b 2


