#! /usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function
import os
import argparse
import time
import spimage
import h5py
import numpy as np


def shape_3d(vol):
    l = int(np.round(np.power(vol.shape[0], 1./3)))
    return vol.reshape((l, l, l))


def generate_mask(res, rad_min=0, rad_max=0):
    (res_x, res_y, res_z) = np.mgrid[0:res, 0:res, 0:res]
    res_x -= res//2
    res_y -= res//2
    res_z -= res//2
    radius = np.sqrt(res_x*res_x + res_y*res_y + res_z*res_z)

    if rad_max == 0:
        rad_max = np.amax(radius)

    mask = np.zeros_like(radius, dtype=bool)
    mask[(radius > rad_min) & (radius <= rad_max)] = True

    return mask


def apply_binning(data, bin_value):
    Z,Y,X = data.shape

    newX = X//bin_value
    newY = Y//bin_value
    newZ = Z//bin_value

    new_data = data[:newZ*bin_value,:newY*bin_value,:newX*bin_value].reshape(newZ,bin_value,newY,bin_value,newX,bin_value).sum(axis=(1,3,5))

    return new_data


def main():
    parser = argparse.ArgumentParser(description="Phase retrieval")
    parser.add_argument("bin_file", help="Data to process")
    parser.add_argument("-m", "--min", type=int, help="Minimal q in mask", default=0)
    parser.add_argument("-M", "--max", type=int, help="Maximal q in mask", default=0)
    parser.add_argument("-b", "--bin", type=int, help="Binning", default=0)
    args = parser.parse_args()

    data_file = args.bin_file
    q_min = args.min
    q_max = args.max
    binning = args.bin

    if not os.path.exists(data_file):
        parser.error('File {} do not exist'.format(data_file))

    data = shape_3d(np.fromfile(data_file, dtype=np.float64))

    if binning > 0:
        data = apply_binning(data, binning)
        q_min /= binning
        q_max /= binning

    res = data.shape[0]

    print('Input file =', data_file)
    print('Data_size =', res)
    print('Binning =', binning)

    # Phasing parameters
    niter_hio = 500
    niter_er = 50
    niter_total = niter_hio + niter_er
    nrepeat = 2
    beta = 0.9
    support_size = 40.

    print('Total iterations =', niter_total*nrepeat)

    if q_max == 0:
        q_max = res//2

    inner_mask = generate_mask(res, rad_min=q_min)
    outer_mask = generate_mask(res, rad_max=q_max)

    mask = inner_mask*outer_mask

    data[inner_mask==False] = np.amax(data)

    # For model
    # mask = np.empty(data.shape, dtype=bool)
    # mask[:] = True

    start = time.time()
    # Run HIO algorithm with ER at the end
    R = spimage.Reconstructor(use_gpu=True)
    R.set_intensities(data)
    R.set_mask(mask)
    R.set_number_of_iterations(niter_total*nrepeat)
    R.set_number_of_outputs_images(3)
    R.set_number_of_outputs_scores(100)
    R.set_initial_support(radius=support_size/2.)
    # R.set_support_algorithm("static", number_of_iterations=niter_total*nrepeat)
    R.set_support_algorithm("threshold", update_period=10, blur_init=2, \
                            blur_final=1, threshold_init=0.15, threshold_final=0.2)
    # R.set_support_algorithm("area", update_period = 10, blur_init=2, \
    #                         blur_final=1, area_init=target_area*2, area_final=target_area)
    for i in range(nrepeat):
        R.append_phasing_algorithm("hio", beta_init=beta, beta_final=beta,
                                   number_of_iterations=niter_hio)
        R.append_phasing_algorithm("er", number_of_iterations=niter_er)
    output = R.reconstruct()

    #Total reconstruction time
    done = time.time()
    elapsed = done - start
    print('Reconstruction time:', str(elapsed))

    #Save results
    bin_str = 'bin' + str(binning) if binning > 0 else ''
    iter_str = str(niter_total*nrepeat) + 'iter'
    result_name = data_file.split('/')[-1].split('.')[0] + '_phased_'+ bin_str + '_' + iter_str + '.h5'

    with h5py.File(result_name, 'w') as h5file:
        h5file['real_data'] = output['real_space'][-1]
        h5file['fourier_data'] = output['fourier_space'][-1]

        recon_log = h5file.create_group('phase_ret')
        for k, val in output.items():
            if k not in ['real_space', 'fourier_space']:
                recon_log[k] = val


if __name__ == '__main__':
    main()
