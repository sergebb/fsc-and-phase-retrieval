#! /bin/sh

#SBATCH -D /s/ls4/groups/g0065/ikonk/phase_retrieval
#SBATCH -o log/%j.out
#SBATCH -e log/%j.err
#SBATCH -p hpc5-gpu-3d
#SBATCH -n 1
#SBATCH --gres=gpu:1
#SBATCH -t 12:00:00

export HOME=/s/ls4/users/sbobkov

ANACONDA_DIR=/s/ls4/users/sbobkov/anaconda2
SW_HOME=/s/ls4/users/sbobkov/sw

export CUDA_HOME=/s/ls4/sw/cuda-8.0
export JAVA_HOME=/s/ls4/sw/java/current

export CUBLAS_INSTALL_PATH=$CUDA_HOME
export CUBLAS_LIBRARIES="-L"$CUDA_HOME"/lib64 -cublas -lcudart"
export CUBLAS_INCLUDES="-I"$CUDA_HOME"include"

export LD_LIBRARY_PATH=$ANACONDA_DIR/lib:$SW_HOME/lib:/s/ls4/sw/misc/lib:/s/ls4/sw/misc/lib64:$JAVA_HOME/lib:/s/ls4/sw/cudnn/5.1/lib64:/usr/lib64:$CUDA_HOME/lib64:$CUDA_HOME/nvvm/lib64

export LIBRARY_PATH=$CUDA_HOME/lib64:$CUDA_HOME/nvvm/lib64:$LIBRARY_PATH

export PATH=$ANACONDA_DIR/bin:$JAVA_HOME/bin:/s/ls4/sw/misc/bin:/s/ls4/sw/gcc/4.9.4/bin:$CUDA_HOME/bin:$PATH

module load intel-compilers

/s/ls4/sw/glibc/2.17/lib/ld-linux-x86-64.so.2 --library-path /s/ls4/sw/glibc/2.17/lib:$LD_LIBRARY_PATH `which python` -m notebook --ip=$MY_IP --no-browser


