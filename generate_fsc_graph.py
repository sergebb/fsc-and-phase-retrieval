#! /usr/bin/env python
# -*- coding: utf-8 -*-

import os
import h5py
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt

mpl.rcParams['image.cmap'] = 'viridis'


def compute_fcs(data, model, qmin, qmax):
    nz, ny, nx = data.shape
    qx = np.arange(nx) - nx//2
    qy = np.arange(ny) - ny//2
    qz = np.arange(nz) - nz//2

    xx, yy, zz = np.meshgrid(qx, qy, qz)

    qq = np.round(np.sqrt(xx*xx + yy*yy + zz*zz))

    fcs = []
    q_val = np.arange(qmin, qmax)

    for q in q_val:
        data_points = data[qq == q]
        model_points = model[qq == q]
        data_abs = np.abs(data_points)
        model_abs = np.abs(model_points)
        fcs_val = np.sum(np.multiply(data_points, np.conj(model_points)))
        fcs_val /= np.sqrt(np.sum(np.multiply(data_abs, data_abs))*
                           np.sum(np.multiply(model_abs, model_abs))
                          )
        fcs.append(fcs_val)

    return np.array(fcs), q_val


def compute_freq(rad_pixels):
    pixel_size = 0.512
    fel_lambda = 6.2
    det_distance = 300

    angle = np.arctan(rad_pixels * pixel_size / det_distance)
    q_value = 2. * np.sin(0.5 * angle) / fel_lambda
    return q_value


def compute_half_p_res(q_value):
    return 0.5 / q_value


def main():
    # Load data

    with h5py.File('intens_11000_phased_bin2_1100iter.h5', 'r') as h5file:
        fourier_11k = h5file['fourier_data'][:]

    with h5py.File('intens_5000_phased_bin2_1100iter.h5', 'r') as h5file:
        fourier_5k = h5file['fourier_data'][:]

    with h5py.File('intens_100_phased_bin2_1100iter.h5', 'r') as h5file:
        fourier_100 = h5file['fourier_data'][:]

    with h5py.File('model_phased_bin2_1100iter.h5', 'r') as h5file:
        fourier_model = h5file['fourier_data'][:]

    fcs_11k, q_11k = compute_fcs(fourier_11k, fourier_model, 0, fourier_model.shape[0]//2)
    fcs_5k, q_5k = compute_fcs(fourier_5k, fourier_model, 0, fourier_model.shape[0]//2)
    fcs_100, q_100 = compute_fcs(fourier_100, fourier_model, 0, fourier_model.shape[0]//2)

    fig, ax = plt.subplots(1, 1, figsize=(12,6))

    ax.plot(compute_freq(q_11k*2)*10, np.abs(fcs_11k), label='11000 images')
    ax.plot(compute_freq(q_5k*2)*10, np.abs(fcs_5k), label='5000 images')
    ax.plot(compute_freq(q_100*2)*10, np.abs(fcs_100), label='100 images')
    ax.set_ylim(0, 1.1)
    ax.set_xlabel(r'Spatial Frequency $[\AA^{-1}]$', fontsize=14)
    ax.set_ylabel('FSC', fontsize=14)
    ax.legend(fontsize=14)

    ax2 = ax.twiny()
    ax2.set_xlim(ax.get_xlim())
    qval_ticks = ax.get_xticks()[1:] # Except 0
    res_ticks = compute_half_p_res(qval_ticks/10)

    ax2.set_xticks(qval_ticks)
    ax2.set_xticklabels(['{:.1f}'.format(r) for r in res_ticks])
    ax2.set_xlabel('Resolution $[nm]$', fontsize=14)

    plt.savefig('fsc.png', dpi=300, bbox_inches='tight')

if __name__ == '__main__':
    main()
